import type { Data, DeepPartial, Engine, Helper, Site } from "lume/core.ts";
import { merge } from "lume/core/utils.ts";
import loader from "lume/core/loaders/text.ts";
import { instantiate } from "./rst_render/lib.ts";

export interface Options {
  /** The list of extensions this plugin applies to */
  extensions: string[];

  /** Options passed to markdown-it library */
  // options: MarkdownItOptions;

  /** The list of markdown-it plugins to use */
  plugins: unknown[];

  /** To modify existing rules or new custom rules */
  // deno-lint-ignore no-explicit-any
  rules: Record<string, (...args: any[]) => any>;

  /** Set `true` append your plugins to the defaults */
  keepDefaultPlugins: boolean;
}

// Default options
export const defaults: Options = {
  extensions: [".rst"],
  // options: { html: true, },
  plugins: [],
  rules: {},
  keepDefaultPlugins: false,
};

/** Template engine to render Markdown files */
export class RstEngine implements Engine {
  constructor() {
  }

  deleteCache() {}

  render(
    content: string,
    _data?: Data,
    _filename?: string,
  ): Promise<string> {
    return instantiate().then(({ render_rst }) => render_rst(content, true));
  }

  renderInline(content: string): Promise<string> {
    return instantiate().then(({ render_rst }) => render_rst(content, false));
  }

  renderSync(
    _content: unknown,
    _data?: Data,
    _filename?: string,
  ): string {
    // if (typeof content !== "string") {
    //   content = String(content);
    // }
    // return this.engine.render(content as string, { filename, data });
    throw new Error("Not Emplements");
  }

  addHelper() {}
}

export default function (userOptions?: DeepPartial<Options>) {
  const options = merge(defaults, userOptions);

  if (options.keepDefaultPlugins && userOptions?.plugins?.length) {
    options.plugins = defaults.plugins.concat(userOptions.plugins);
  }

  return function (site: Site) {
    // @ts-ignore: This expression is not callable.
    // const engine = markdownIt(options.options);

    // Register markdown-it plugins
    // options.plugins.forEach((plugin) =>
    //   Array.isArray(plugin) ? engine.use(...plugin) : engine.use(plugin)
    // );

    // Register custom rules
    // for (const [name, rule] of Object.entries(options.rules)) {
    //   engine.renderer.rules[name] = rule;
    // }

    // Load the pages
    const engine = new RstEngine();
    site.loadPages(options.extensions, loader, engine);

    function filter(string: string, inline = false): Promise<string> {
      if (inline) {
        return engine.renderInline(string?.toString() || "").then((doc) =>
          doc.trim()
        );
      }
      return engine.render(string?.toString() || "").then((doc) => doc.trim());
    }

    // Register the md filter
    site.filter("rst", filter as Helper, true);
  };
}
