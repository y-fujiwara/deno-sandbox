import lume from "lume/mod.ts";
import date from "lume/plugins/date.ts";
import code_highlight from "lume/plugins/code_highlight.ts";
import postcss from "lume/plugins/postcss.ts";
import base_path from "lume/plugins/base_path.ts";
import slugify_urls from "lume/plugins/slugify_urls.ts";
import resolve_urls from "lume/plugins/resolve_urls.ts";
// import netlify_cms from "lume/plugins/netlify_cms.ts";
import pagefind from "lume/plugins/pagefind.ts";
import rst_render from "./plugins/rst_render.ts";

const site = lume();

site.use(date());
site.use(code_highlight());
site.use(postcss());
site.use(base_path());
site.use(slugify_urls());
site.use(resolve_urls());
// site.use(netlify_cms());
site.use(pagefind());
site.use(rst_render());

export default site;
