import {
  Canvas,
  EventType,
  Window,
  WindowBuilder,
} from "https://deno.land/x/sdl2@0.9.0/mod.ts";
import { GRID_SIZE, lifeGameVertices } from "./lifeGame.ts";

const win: Window = new WindowBuilder("Your First WebGPU", 512, 512).build();

const adapter = await navigator.gpu.requestAdapter();
if (!adapter) {
  console.error("adapter is null");
  Deno.exit();
}
const device = await adapter.requestDevice();
const surface: Deno.UnsafeWindowSurface = win.windowSurface();

// フォーマットをcontextの設定として用います
const context = surface.getContext("webgpu");
context.configure({
  device,
  format: navigator.gpu.getPreferredCanvasFormat(),
  width: 512,
  height: 512,
});

// JavaScriptの配列を直接はGPUで扱えないので、それ用に変換
const vertexBuffer = device.createBuffer({
  label: "Cell vertices",
  size: lifeGameVertices.byteLength,
  // TypeScriptのUnionではなく、ビット演算子のOR
  usage: GPUBufferUsage.VERTEX | GPUBufferUsage.COPY_DST,
});
device.queue.writeBuffer(vertexBuffer, 0, lifeGameVertices);

// 上記の段階だとただ単にGPUにバイト配列を与えただけなので、実際にほしい構造を定義する
const vertexBufferLayout: GPUVertexBufferLayout = {
  arrayStride: 8,
  attributes: [{
    format: "float32x2",
    offset: 0,
    shaderLocation: 0, // Position. Matches @location(0) in the @vertex shader.
  }],
};

// 頂点シェーダーの定義、先の配列の頂点ごとに一回ずつ並列に呼び出される(今回は六回)
const cellShaderModule = device.createShaderModule({
  label: "Cell shader",
  code: `
struct VertexOutput {
  @builtin(position) position: vec4f,
  @location(0) cell: vec2f,
};

@group(0) @binding(0) var<uniform> grid: vec2f;
// 実態はBooleanだけど、GPUのメモリレイアウトの都合でu32のほうが早いらしい
@group(0) @binding(1) var<storage> cellState: array<u32>; 

@vertex
fn vertexMain(@location(0) position: vec2f,
              @builtin(instance_index) instance: u32) -> VertexOutput {
  let i = f32(instance);
  let cell = vec2f(i % grid.x, floor(i / grid.x));
  let state = f32(cellState[instance]);

  let cellOffset = cell / grid * 2.0;
  let gridPos = (position*state+1.0) / grid - 1.0 + cellOffset;

  var output: VertexOutput;
  output.position = vec4f(gridPos.x, gridPos.y, 0, 1);
  output.cell = cell;
  return output;
}

// フラグメントシェーダーは頂点シェーダーで作られる三角形をラスタライズして、含まれる各ピクセルに対して一回呼ばれる
@fragment
fn fragmentMain(input: VertexOutput) -> @location(0) vec4f {
  // Remember, fragment return values are (Red, Green, Blue, Alpha)
  // and since cell is a 2D vector, this is equivalent to:
  // (Red = cell.x, Green = cell.y, Blue = 0, Alpha = 1)
  // カラースケールが0~1の範囲で、x,yは32なので切り詰められる。それを防ぐための除算
  // let c = input.cell / grid;
  // 左下が黒くなるので、青をベースにするようにしている
  // return vec4f(c, 1-c.x, 1);
  return vec4f(input.cell.x, input.cell.y, 1.0 - input.cell.x, 1);
}
`,
});

// シェーダーはレンダリングパイプラインを通して実行される 実際にシェーダーをどのように扱うか定義する
const cellPipeline = device.createRenderPipeline({
  label: "Cell pipeline",
  layout: "auto",
  vertex: {
    module: cellShaderModule,
    entryPoint: "vertexMain",
    buffers: [vertexBufferLayout],
  },
  fragment: {
    module: cellShaderModule,
    entryPoint: "fragmentMain",
    targets: [{
      format: navigator.gpu.getPreferredCanvasFormat(),
    }],
  },
});
// グリッドのサイズをシェーダーに伝える必要があるので、グリッドのサイズが変更されるたびにシェーダー再作成が必要で非効率なのを解消するためにユニフォームバッファを使う
// 一般的に共有の処理をユニフォームバッファとするらしい(全体的な設定とか時刻とか)
// 基本書き込みもできないし、サイズ制限もあり、固定長なので定数のような使い方
const uniformArray = new Float32Array([GRID_SIZE, GRID_SIZE]);
const uniformBuffer = device.createBuffer({
  label: "Grid Uniforms",
  size: uniformArray.byteLength,
  usage: GPUBufferUsage.UNIFORM | GPUBufferUsage.COPY_DST,
});
device.queue.writeBuffer(uniformBuffer, 0, uniformArray);

// 各セルの状態を保存するためのストレージバッファ
const cellStateArray = new Uint32Array(GRID_SIZE * GRID_SIZE);
const cellStateStorage = [
  device.createBuffer({
    label: "Cell State A",
    size: cellStateArray.byteLength,
    usage: GPUBufferUsage.STORAGE | GPUBufferUsage.COPY_DST,
  }),
  device.createBuffer({
    label: "Cell State B",
    size: cellStateArray.byteLength,
    usage: GPUBufferUsage.STORAGE | GPUBufferUsage.COPY_DST,
  }),
];

// Mark every third cell of the first grid as active.
for (let i = 0; i < cellStateArray.length; i += 3) {
  cellStateArray[i] = 1;
}
device.queue.writeBuffer(cellStateStorage[0], 0, cellStateArray);

// Mark every other cell of the second grid as active.
for (let i = 0; i < cellStateArray.length; i++) {
  cellStateArray[i] = i % 2;
}
device.queue.writeBuffer(cellStateStorage[1], 0, cellStateArray);

const bindGroups = [
  device.createBindGroup({
    label: "Cell renderer bind group A",
    layout: cellPipeline.getBindGroupLayout(0),
    entries: [{
      binding: 0,
      resource: { buffer: uniformBuffer },
    }, {
      binding: 1,
      resource: { buffer: cellStateStorage[0] },
    }],
  }),
  device.createBindGroup({
    label: "Cell renderer bind group B",
    layout: cellPipeline.getBindGroupLayout(0),
    entries: [{
      binding: 0,
      resource: { buffer: uniformBuffer },
    }, {
      binding: 1,
      resource: { buffer: cellStateStorage[1] },
    }],
  }),
];

let step = 0;
for await (const event of win.events(false)) {
  step++;
  if (event.type === EventType.Quit) break;
  // コマンドはすべてレンダリングパスを通して実行されるのでその準備
  const renderPassDescriptor: GPURenderPassDescriptor = {
    colorAttachments: [{
      view: context.getCurrentTexture().createView(),
      loadOp: "clear",
      clearValue: { r: 0, g: 0, b: 0.4, a: 1.0 },
      storeOp: "store",
    }],
  };
  const encoder = device.createCommandEncoder();
  const pass = encoder.beginRenderPass(renderPassDescriptor);
  pass.setPipeline(cellPipeline);
  console.log(bindGroups[step % 2]);
  pass.setVertexBuffer(0, vertexBuffer);

  // vertices => 配列の要素数は12だが、実際にはx,yの組み合わせなので頂点数としては2で割る
  // 第に引数によってシステムに対して、正方形の 6 個（vertices.length / 2）の頂点を 16 回（GRID_SIZE * GRID_SIZE）描画するよう指示
  pass.draw(lifeGameVertices.length / 2, GRID_SIZE * GRID_SIZE);

  // レンダリングパスの修了合図
  pass.end();

  // コマンドを指定の順で実行するためにキューに追加 送信するとコマンドバッファは使えなくなるので、実際は↑のものを直接引数にいれる
  // device.queue.submit([encoder.finish()]);
  console.log(step);
  device.queue.submit([encoder.finish()]);

  surface.present();
}
