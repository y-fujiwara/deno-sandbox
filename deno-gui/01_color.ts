import {
  EventType,
  Window,
  WindowBuilder,
} from "https://deno.land/x/sdl2@0.9.0/mod.ts";

const win: Window = new WindowBuilder("Hello, World!", 800, 600).build();

const adapter = await navigator.gpu.requestAdapter();
if (!adapter) {
  console.error("adapter is null");
  Deno.exit();
}
const device = await adapter.requestDevice();

const surface: Deno.UnsafeWindowSurface = win.windowSurface();

// フォーマットをcontextの設定として用います
const context = surface.getContext("webgpu");
context.configure({
  device,
  format: navigator.gpu.getPreferredCanvasFormat(),
  width: 800,
  height: 600,
});

for await (const event of win.events(false)) {
  if (event.type === EventType.Quit) break;

  // Sine wave
  const r = Math.sin(Date.now() / 1000) / 2 + 0.5;
  const g = Math.sin(Date.now() / 1000 + 2) / 2 + 0.5;
  const b = Math.sin(Date.now() / 1000 + 4) / 2 + 0.5;

  const textureView = context.getCurrentTexture().createView();
  const renderPassDescriptor: GPURenderPassDescriptor = {
    colorAttachments: [
      {
        view: textureView,
        clearValue: { r, g, b, a: 1.0 },
        loadOp: "clear",
        storeOp: "store",
      },
    ],
  };

  const commandEncoder = device.createCommandEncoder();
  const passEncoder = commandEncoder.beginRenderPass(renderPassDescriptor);
  passEncoder.end();

  device.queue.submit([commandEncoder.finish()]);
  surface.present();
}

/*
// シェーダー付き
import {
  EventType,
  Window,
  WindowBuilder,
} from "https://deno.land/x/sdl2@0.9.0/mod.ts";

const win: Window = new WindowBuilder("Hello, World!", 800, 600).build();
const adapter = await navigator.gpu.requestAdapter();

if (!adapter) {
  console.error("adapter is null");
  Deno.exit();
}

const device = await adapter.requestDevice();
const surface: Deno.UnsafeWindowSurface = win.windowSurface();
const context = surface.getContext("webgpu");
context.configure({ device, format: "bgra8unorm", width: 800, height: 600 });

// 頂点シェーダー（WGSL）
const vertexShaderSource = `
struct VertexOutput {
  [[builtin(position)]] Position : vec4<f32>;
};

[[stage(vertex)]]
fn main([[builtin(vertex_index)]] VertexIndex : u32) -> VertexOutput {
  var pos : array<vec2<f32>, 3> = array<vec2<f32>, 3>(
    vec2<f32>(0.0, 0.5),
    vec2<f32>(-0.5, -0.5),
    vec2<f32>(0.5, -0.5));

  return VertexOutput(
    vec4<f32>(pos[VertexIndex], 0.0, 1.0));
}
`;

// フラグメントシェーダー（WGSL）
const fragmentShaderSource = `
[[stage(fragment)]]
fn main() -> [[location(0)]] vec4<f32> {
  return vec4<f32>(1.0, 0.0, 0.0, 1.0);
}
`;

// シェーダーモジュールの作成
const vertexShaderModule = device.createShaderModule({
  code: vertexShaderSource,
});
const fragmentShaderModule = device.createShaderModule({
  code: fragmentShaderSource,
});

// パイプラインの作成
const pipeline = device.createRenderPipeline({
  layout: "auto",
  vertex: { module: vertexShaderModule, entryPoint: "main" },
  fragment: {
    module: fragmentShaderModule,
    entryPoint: "main",
    targets: [{ format: "bgra8unorm" }],
  },
});

for await (const event of win.events(true)) {
  if (event.type == EventType.Quit) break;

  // Sine wave
  const r = Math.sin(Date.now() / 1000) / 2 + 0.5;
  const g = Math.sin(Date.now() / 1000 + 2) / 2 + 0.5;
  const b = Math.sin(Date.now() / 1000 + 4) / 2 + 0.5;

  const textureView = context.getCurrentTexture().createView();
  const commandEncoder = device.createCommandEncoder();

  // レンダーパスの作成とシェーダーの使用
  const passEncoder = commandEncoder.beginRenderPass({
    colorAttachments: [{
      view: textureView,
      loadOp: "clear",
      storeOp: "store",
      clearValue: { r, g, b, a: 1.0 },
    }],
    // pipelineをここに追加
  });
  passEncoder.end();

  device.queue.submit([commandEncoder.finish()]);

  surface.present();
}
*/
