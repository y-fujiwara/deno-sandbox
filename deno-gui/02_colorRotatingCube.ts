// シェーダー付き
import {
  EventType,
  Window,
  WindowBuilder,
} from "https://deno.land/x/sdl2@0.9.0/mod.ts";
// @deno-types="https://wgpu-matrix.org/dist/2.x/wgpu-matrix.d.ts"
import {
  mat4,
  vec3,
} from "https://wgpu-matrix.org/dist/2.x/wgpu-matrix.module.js";
import {
  cubePositionOffset,
  cubeUVOffset,
  cubeVertexArray,
  cubeVertexCount,
  cubeVertexSize,
} from "./cube.ts";

const win: Window = new WindowBuilder("Color Cube", 800, 600).build();
const adapter = await navigator.gpu.requestAdapter();

if (!adapter) {
  console.error("adapter is null");
  Deno.exit();
}

const device = await adapter.requestDevice();
const surface: Deno.UnsafeWindowSurface = win.windowSurface();
const context = surface.getContext("webgpu");
context.configure({
  device,
  format: "bgra8unorm",
  /* alphaMode: "premultiplied", */ width: 800,
  height: 600,
});

// 頂点シェーダー（WGSL）
const vertexShaderSource = `
struct Uniforms {
  modelViewProjectionMatrix : mat4x4<f32>,
}
@binding(0) @group(0) var<uniform> uniforms : Uniforms;

struct VertexOutput {
  @builtin(position) Position : vec4<f32>,
  @location(0) fragUV : vec2<f32>,
  @location(1) fragPosition: vec4<f32>,
}

@vertex
fn main(
  @location(0) position : vec4<f32>,
  @location(1) uv : vec2<f32>
) -> VertexOutput {
  var output : VertexOutput;
  output.Position = uniforms.modelViewProjectionMatrix * position;
  output.fragUV = uv;
  output.fragPosition = 0.5 * (position + vec4(1.0, 1.0, 1.0, 1.0));
  return output;
}`;

// フラグメントシェーダー（WGSL）
const fragmentShaderSource = `
@fragment
fn main(
  @location(0) fragUV: vec2<f32>,
  @location(1) fragPosition: vec4<f32>
) -> @location(0) vec4<f32> {
  return fragPosition;
}`;

// Create a vertex buffer from the cube data.
const verticesBuffer = device.createBuffer({
  size: cubeVertexArray.byteLength,
  usage: GPUBufferUsage.VERTEX,
  mappedAtCreation: true,
});
new Float32Array(verticesBuffer.getMappedRange()).set(cubeVertexArray);
verticesBuffer.unmap();

// シェーダーモジュールの作成
const vertexShaderModule = device.createShaderModule({
  code: vertexShaderSource,
});
const fragmentShaderModule = device.createShaderModule({
  code: fragmentShaderSource,
});
// パイプラインの作成
const pipeline = device.createRenderPipeline({
  layout: "auto",
  vertex: {
    module: vertexShaderModule,
    entryPoint: "main",
    buffers: [
      {
        arrayStride: cubeVertexSize,
        attributes: [
          {
            // position
            shaderLocation: 0,
            offset: cubePositionOffset,
            format: "float32x4",
          },
          {
            // uv
            shaderLocation: 1,
            offset: cubeUVOffset,
            format: "float32x2",
          },
        ],
      },
    ],
  },
  fragment: {
    module: fragmentShaderModule,
    entryPoint: "main",
    targets: [{ format: "bgra8unorm" }],
  },
  primitive: { topology: "triangle-list", cullMode: "back" },
  depthStencil: {
    depthWriteEnabled: true,
    depthCompare: "less",
    format: "depth24plus",
  },
});

const depthTexture = device.createTexture({
  size: [800, 600],
  format: "depth24plus",
  usage: GPUTextureUsage.RENDER_ATTACHMENT,
});

const uniformBufferSize = 4 * 16; // 4x4 matrix
const uniformBuffer = device.createBuffer({
  size: uniformBufferSize,
  usage: GPUBufferUsage.UNIFORM | GPUBufferUsage.COPY_DST,
});
const uniformBindGroup = device.createBindGroup({
  layout: pipeline.getBindGroupLayout(0),
  entries: [
    {
      binding: 0,
      resource: {
        buffer: uniformBuffer,
      },
    },
  ],
});

const aspect = 800 / 600;
const projectionMatrix = mat4.perspective(
  (2 * Math.PI) / 5,
  aspect,
  1,
  100.0,
);
const modelViewProjectionMatrix = mat4.create();

function getTransformationMatrix() {
  const viewMatrix = mat4.identity();
  mat4.translate(viewMatrix, vec3.fromValues(0, 0, -4), viewMatrix);
  const now = Date.now() / 1000;
  mat4.rotate(
    viewMatrix,
    vec3.fromValues(Math.sin(now), Math.cos(now), 0),
    1,
    viewMatrix,
  );

  mat4.multiply(projectionMatrix, viewMatrix, modelViewProjectionMatrix);

  return modelViewProjectionMatrix as Float32Array;
}

// eventからマウス位置を取って、座標位置からcube回転させればマウスでコントロール可能だが、幾何計算が必要なのライブラリとかに頼りたい
// 既存のWeb資産(npmとか)にあるにはあるが、ターゲットがCanvasとかで、Canvas自体へのイベント追加とかで動くのでそのままだと流用できない(3d-view-controlsとか)
// surfaceがAPI的にcanvasのWrapperだとラクできそうだけど・・・・
for await (const event of win.events(false)) {
  if (event.type === EventType.Quit) break;

  const transformationMatrix = getTransformationMatrix();
  device.queue.writeBuffer(
    uniformBuffer,
    0,
    transformationMatrix.buffer,
    transformationMatrix.byteOffset,
    transformationMatrix.byteLength,
  );
  const renderPassDescriptor: GPURenderPassDescriptor = {
    colorAttachments: [
      {
        view: context.getCurrentTexture().createView(),
        clearValue: { r: 0.5, g: 0.5, b: 0.5, a: 1.0 },
        loadOp: "clear",
        storeOp: "store",
      },
    ],
    depthStencilAttachment: {
      view: depthTexture.createView(),

      depthClearValue: 1.0,
      depthLoadOp: "clear",
      depthStoreOp: "store",
    },
  };

  const commandEncoder = device.createCommandEncoder();
  const passEncoder = commandEncoder.beginRenderPass(renderPassDescriptor);
  passEncoder.setPipeline(pipeline);
  passEncoder.setBindGroup(0, uniformBindGroup);
  passEncoder.setVertexBuffer(0, verticesBuffer);
  passEncoder.draw(cubeVertexCount);
  passEncoder.end();
  device.queue.submit([commandEncoder.finish()]);

  surface.present();
}
