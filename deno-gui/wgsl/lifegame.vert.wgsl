struct VertexOutput { 
  @builtin(position) position: vec4f, 
  @location(0) cell: vec2f, 
};

@group(0) @binding(0) var<uniform> grid: vec2f;
@group(0) @binding(1) var<storage> cellState: array<u32>;

@vertex
fn vertexMain(@location(0) position: vec2f,
    @builtin(instance_index) instance: u32) -> VertexOutput {
    var output: VertexOutput;

    let i = f32(instance);
    let cell = vec2f(i % grid.x, floor(i / grid.x));

    let scale = f32(cellState[instance]);
    let cellOffset = cell / grid * 2.0;
    let gridPos = (position * scale + 1.0) / grid - 1.0 + cellOffset;

    output.position = vec4f(gridPos.x, gridPos.y, 0, 1);
    output.cell = cell / grid;
    return output;
}

@fragment
fn fragmentMain(input: VertexOutput) -> @location(0) vec4f {
    return vec4f(input.cell.x, input.cell.y, 1.0 - input.cell.x, 1);
}
