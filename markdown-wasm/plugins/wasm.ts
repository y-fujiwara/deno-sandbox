import {
  Plugin,
  PluginRenderResult,
  PluginRenderScripts,
} from "$fresh/server.ts";

export interface Options {
  jsName: string;
  wasmName: string;
}

export default function wasmPlugin(options?: Options): Plugin {
  const main = `data:application/javascript,import { instantiate } from "${
    new URL("../lib/rs_lib.generated.js", import.meta.url).href
  }";export default async function() { await instantiate({url: new URL("/rs_lib_bg.wasm", location.origin)});}`;
  return {
    name: "wasmPlugin",
    entrypoints: { "main": main },
    render(ctx): PluginRenderResult {
      const res = ctx.render();
      const scripts: PluginRenderScripts[] = [];
      if (res.requiresHydration) {
        scripts.push({ entrypoint: "main", state: {} });
      }
      return { scripts, styles: [] };
    },
  };
}
