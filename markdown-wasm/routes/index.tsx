import { Head } from "$fresh/runtime.ts";
import Add from "../islands/Add.tsx";
import Counter from "../islands/Counter.tsx";
import { add } from "../lib/rs_lib.generated.js";

export default function Home() {
  const addResult = add(100, 1);
  return (
    <>
      <Head>
        <title>Fresh App</title>
      </Head>
      <div>
        <img
          src="/logo.svg"
          width="128"
          height="128"
          alt="the fresh logo: a sliced lemon dripping with juice"
        />
        <p>
          Welcome to `fresh`. Try updating this message in the
          ./routes/index.tsx file, and refresh.
        </p>
        <Counter start={3} />
        <hr />
        <span>server side wasm add result: {addResult}</span>
        <Add />
      </div>
    </>
  );
}
