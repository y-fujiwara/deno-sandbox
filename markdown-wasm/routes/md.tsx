import { Head } from "$fresh/runtime.ts";
import Markdown from "../islands/Markdown.tsx";
import { parse_markdown } from "../lib/rs_lib.generated.js";

const README = `
# WebAssemblyでマークダウンライブラリを作る

この文章はサーバーサイドでWebAssemblyを利用して、マークダウンから作られたものです。

利用技術は以下になります。

- Rust
- [pulldown_cmark](https://crates.io/crates/pulldown-cmark)
- deno
  - fresh
`;

export default function Md() {
  const mdHTML = parse_markdown(README);
  return (
    <>
      <Head>
        <title>Fresh App</title>
      </Head>
      <div dangerouslySetInnerHTML={{__html: mdHTML}}>
      </div>
      <Markdown />
    </>
  );
}
