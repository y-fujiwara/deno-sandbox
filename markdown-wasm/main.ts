/// <reference no-default-lib="true" />
/// <reference lib="dom" />
/// <reference lib="dom.iterable" />
/// <reference lib="dom.asynciterable" />
/// <reference lib="deno.ns" />

import { start } from "$fresh/server.ts";
import manifest from "./fresh.gen.ts";
import { instantiate } from "./lib/rs_lib.generated.js";
import wasmPlugin from "./plugins/wasm.ts";

await instantiate();
await start(manifest, {
  plugins: [
    // This line configures Fresh to use the first-party twind plugin.
    // wasmPlugin(),
  ],
});
