use ammonia::clean;
use pulldown_cmark::{html, Options, Parser};
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub fn add(a: i32, b: i32) -> i32 {
  return a + b;
}

#[wasm_bindgen]
pub fn parse_markdown(markdown_input: &str) -> String {
  let mut options = Options::empty();
  options.insert(Options::ENABLE_STRIKETHROUGH);
  let parser = Parser::new_ext(markdown_input, options);

  let mut html_output = String::new();
  html::push_html(&mut html_output, parser);
  clean(&*html_output)
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn it_works() {
    let result = add(1, 2);
    assert_eq!(result, 3);
  }

  #[test]
  fn parse_test() {
    let html_output = parse_markdown(
      "Hello world, this is a ~~complicated~~ *very simple* example.",
    );

    let expected_html = "<p>Hello world, this is a <del>complicated</del> <em>very simple</em> example.</p>\n";
    assert_eq!(expected_html, &html_output);
  }

  #[test]
  fn parse_xss_test() {
    let html_output = parse_markdown("XSS<script>alert(1);</script>");

    let expected_html = "<p>XSS</p>\n";
    assert_eq!(expected_html, &html_output);
  }
}
