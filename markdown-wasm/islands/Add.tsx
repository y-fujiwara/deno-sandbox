import { useEffect, useState } from "preact/hooks";
import { JSX } from "preact/jsx-runtime";
import { Button } from "../components/Button.tsx";
import { add, instantiate } from "../lib/rs_lib.generated.js";

interface State {
  count: number;
  addLeft: number;
  addRight: number;
  isWasmReady: boolean;
}

export default function Add() {
  const [state, setState] = useState<State>({
    count: 0,
    addLeft: 0,
    addRight: 0,
    isWasmReady: false,
  });

  useEffect(() => {
    instantiate({ url: new URL("/rs_lib_bg.wasm", location.origin) }).then(
      () => {
        setState({ ...state, isWasmReady: true });
      },
    );
  }, []);

  const onClick = () => {
    if (!state.isWasmReady) return;

    const count = add(state.addLeft, state.addRight);
    setState({
      ...state,
      count: count,
    });
  };

  const onLeftChange = (e: JSX.TargetedEvent<HTMLInputElement>) => {
    setState({ ...state, addLeft: Number(e.currentTarget.value) || 0 });
  };

  const onRightChange = (e: JSX.TargetedEvent<HTMLInputElement>) => {
    setState({ ...state, addRight: Number(e.currentTarget.value) || 0 });
  };

  return (
    <div>
      <div>
        <Button disabled={!state.isWasmReady} onClick={onClick}>
          足し算を実行する
        </Button>
      </div>
      <input type="number" value={state.addLeft} onInput={onLeftChange} />
      +
      <input type="number" value={state.addRight} onInput={onRightChange} />
      =
      <span>{state.count}</span>
    </div>
  );
}
