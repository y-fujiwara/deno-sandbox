import { useEffect, useState } from "preact/hooks";
import { JSX } from "preact/jsx-runtime";
import { instantiate, parse_markdown } from "../lib/rs_lib.generated.js";

interface State {
  md: string;
  html: string;
  isWasmReady: boolean;
}

const TEXTAREA_STYLE = {
	width: 500,
	height: 800
};

export default function Add() {
  const [state, setState] = useState<State>({
    md: "",
    html: "",
    isWasmReady: false,
  });

  useEffect(() => {
    instantiate({ url: new URL("/rs_lib_bg.wasm", location.origin) }).then(
      () => {
        setState({ ...state, isWasmReady: true });
      },
    );
  }, []);

  const onInput = (e: JSX.TargetedEvent<HTMLTextAreaElement>) => {
    if (!state.isWasmReady) return;

    const md = e.currentTarget.value;
    const html = parse_markdown(md);
    setState({ ...state, html, md });
  };

  return (
    <div style={{display: "flex"}}>
      <textarea style={TEXTAREA_STYLE} disabled={!state.isWasmReady} value={state.md} onInput={onInput} />
      <div dangerouslySetInnerHTML={{ __html: state.html }}></div>
    </div>
  );
}
