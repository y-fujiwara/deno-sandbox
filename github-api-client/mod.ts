import { config } from "./deps.ts";

const token = config().GITHUB_TOKEN;
const user = config().GITHUB_USER;

function buildConfig(): RequestInit {
  return {
    headers: {
      Authorization: `token ${token}`,
      Accept: "application/vnd.github.v3+json",
    },
  };
}

async function getRepos(): Promise<any> {
  const res = await fetch(
    `https://api.github.com/users/${user}/repos`,
    buildConfig(),
  );
  return await res.json();
}

async function getPulls(repo: string): Promise<any> {
  const res = await fetch(
    `https://api.github.com/repos/${user}/${repo}/pulls?state=all`,
    buildConfig(),
  );
  return await res.json();
}

async function getComments(repo: string, id: number): Promise<any> {
  const res = await fetch(
    `https://api.github.com/repos/${user}/${repo}/pulls/${id}/comments`,
    buildConfig(),
  );
}

const repos = await getRepos() as any[];

repos.forEach(async (repo) => {
  console.log(await getPulls(repo.name));
});
