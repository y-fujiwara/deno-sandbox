import setting from "./setting.json" assert { type: "json" };

async function outputVue2React(filepath: string, code: string, css: string) {
}

async function transform(target: string): Promise<string> {
  console.log(target);
  return target;
}

async function readFile(path: string) {
  const text = await Deno.readTextFile(path);
  return text;
}

async function readDirFile(baseDir: string, targetExt: string) {
  const pattern = new RegExp(`.${targetExt}$`);
  for await (const dirEntry of Deno.readDir(baseDir)) {
    if (dirEntry.isDirectory) {
      await readDirFile(`${baseDir}/${dirEntry.name}`, targetExt);
    } else if (dirEntry.isFile) {
      const filePath = `${baseDir}/${dirEntry.name}`;
      if (!pattern.test(filePath)) {
        continue;
      }
      const text = await readFile(filePath);
      const res = await transform(text);
      // TODO: implement
      await outputVue2React("", "", "");
    }
  }
}

// Learn more at https://deno.land/manual/examples/module_metadata#concepts
if (import.meta.main) {
  // TODO: 環境変数化など
  console.log(setting.settings);
  readDirFile("./sample", "vue");
}
