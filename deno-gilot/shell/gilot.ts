const BASE_FOLDER = "./outputs";
const GILOT_COMMAND = "gilot";

export async function plotGilot(repository: string) {
  const fullPath = `${BASE_FOLDER}/${repository}`;
  const dataFilePath = `${fullPath}_data.csv`;
  const graphFilePath = `${fullPath}_graph.png`;

  await buildDir(fullPath);

  const command = new Deno.command(GILOT_COMMAND, {
    args: ["plot", "-i", dataFilePath, "-o", graphFilePath],
  });
  console.log(`Plotting gilot for repository: ${repository}`);
}

/**
 * Build a directory at the given path if it doesn't already exist.
 *
 * @param {string} path - The path to the directory to build.
 *
 * @return {Promise<void>} - A promise that resolves when the directory has been built.
 */
async function buildDir(path: string) {
  if (await folderExists(path)) {
    return;
  }
  await Deno.mkdir(path, true);
}

async function folderExists(path: string): Promise<boolean> {
  try {
    const file = await Deno.stat(path);
    return file.isDirectory();
  } catch (e) {
    return false;
  }
}