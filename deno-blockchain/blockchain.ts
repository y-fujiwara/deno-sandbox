export abstract class Blockchain {
  readonly chain;
  readonly curentTransaction;

  constructor() {
    this.chain = [];
    this.currentTransaction = [];
  }

  static hash(block): any;

  get lastBlock(): any;

}
