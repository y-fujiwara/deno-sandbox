import { mysql_close, mysql_init, mysql_real_connect } from "../src/ffi.ts";
// TODO: deps.ts
import {
  assertNotEquals,
} from "https://deno.land/std@0.65.0/testing/asserts.ts";
Deno.test("mysql open after close", () => {
  const client = mysql_init(null);
  assertNotEquals(client, null);

  const connectRes = mysql_real_connect(
    client,
    "localhost",
    "root",
    "",
    "mysql",
    3306,
    null,
    0,
  );
  assertNotEquals(connectRes, null);

  mysql_close(client);
});
