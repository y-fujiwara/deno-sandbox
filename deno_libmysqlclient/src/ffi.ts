import { strToBytesWithTerminated } from "./util.ts";

function ffiSuffix(): string {
  switch (Deno.build.os) {
    case "windows":
      return "dll";
    case "darwin":
      return "dylib";
    case "linux":
      return "so";
  }
}
const symbols = <Record<string, Deno.ForeignFunction>> {
  mysql_init: {
    parameters: [
      "pointer", // MYSQL *mysql
    ],
    result: "pointer",
  },
  mysql_real_connect: {
    parameters: [
      "pointer", // MYSQL *mysql
      "pointer", // const char *host
      "pointer", // const char *user
      "pointer", // const char *passwd
      "pointer", // const char *db
      "u32", // unsigned int port
      "pointer", // const char *unix_socket
      // TODO: fix unsigned long
      "f32", // unsigned long client_flag
    ],
    result: "pointer",
  },
  mysql_close: {
    parameters: [
      "pointer", // MYSQL *mysql
    ],
    result: "void",
  },
};

let lib: Deno.DynamicLibrary<typeof symbols>;

const envSqlitePath = Deno.env.get("DENO_LIB_MYSQL_CLIENT_PATH");
if (envSqlitePath !== undefined) {
  lib = Deno.dlopen(envSqlitePath, symbols);
} else {
  try {
    lib = Deno.dlopen(`libmysqlclient.${ffiSuffix()}`, symbols);
  } catch (e) {
    const error = new Error(
      "libmysqlclient is not found.",
    );
    error.cause = e;
    throw error;
  }
}

export type mysql = Deno.UnsafePointer;
export type ffi_string = string | null;

export function mysql_init(client: mysql | null): mysql {
  return lib.symbols.mysql_init(client) as mysql;
}

export function mysql_real_connect(
  client: mysql,
  host: ffi_string,
  user: ffi_string,
  passwd: ffi_string,
  db: ffi_string,
  port: number,
  unix_socket: ffi_string,
  client_flag: number,
): mysql {
  const mysql_host = strToBytesWithTerminated(host);
  const mysql_user = strToBytesWithTerminated(user);
  const mysql_passwd = strToBytesWithTerminated(passwd);
  const mysql_db = strToBytesWithTerminated(db);
  const mysql_unix_socket = strToBytesWithTerminated(unix_socket);

  return lib.symbols.mysql_real_connect(
    client,
    mysql_host,
    mysql_user,
    mysql_passwd,
    mysql_db,
    port,
    mysql_unix_socket,
    client_flag,
  ) as mysql;
}

export function mysql_close(client: mysql): void {
  return lib.symbols.mysql_close(client) as void;
}
