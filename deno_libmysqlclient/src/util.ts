const C_TERMINATED_CHAR = "\0";
export function strToBytesWithTerminated(
  word: string | null,
): Uint8Array | null {
  if (!word) {
    return null;
  }
  return new TextEncoder().encode(`${word}${C_TERMINATED_CHAR}`);
}
