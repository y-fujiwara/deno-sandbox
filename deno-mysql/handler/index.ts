import { Context, Router } from "https://deno.land/x/oak/mod.ts";
import { getIndexUsers, postCreateUser } from "./user.ts";
const router = new Router();
router
  .get("/", (context: Context) => {
    context.response.body = "Hello world!";
  })
  .get("/users", getIndexUsers)
  .post("/user", postCreateUser);

export { router };
