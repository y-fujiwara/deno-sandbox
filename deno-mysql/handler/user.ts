import { indexUser, createUser } from "../repository/user.ts";
import { Context } from "https://deno.land/x/oak/mod.ts";

export const getIndexUsers = async (context: Context) => {
  const users = await indexUser();
  context.response.type = "json";
  context.response.body = users;
};

export const postCreateUser = async (context: Context) => {
  const result = context.request.body({ type: "json" });
  const text = await result.value;

  const res = await createUser(text.name);
  context.response.type = "json";
  context.response.body = res;
};
