# deno mysql docker sample

## prepare

```
$ cp .env.sample .env
$ docker-compose up -d
```

if volume already exists

```
$ docker-compose stop
$ docker-compose rm
$ docker volume rm deno-mysql_db-store
```

## run migration

in docker(`docker-compose exec deno /bin/bash` or `./scripts/docker_shell.sh`)

```
$ deno run -A --unstable https://deno.land/x/nessie/cli.ts migrate
```

## development

run web api server in container

```
$ ./scripts/dev.sh
```

apis

- GET: http://localhost:8000/users
  - index user
- POST: http://localhost:8000/user
  - body: {name: string}
  - ex: `fetch("/user", { method: "POST", body: JSON.stringify({name: "test"})})`

TODO: hot reloading
