import {
    ClientMySQL,
    NessieConfig,
} from "https://deno.land/x/nessie@2.0.0-rc4/mod.ts";
import * as dotEnv from "https://deno.land/x/dotenv/mod.ts";

const envs = dotEnv.config();

/** Select one of the supported clients */
const client = new ClientMySQL({
    hostname: envs["MYSQL_HOST"],
    port: parseInt(envs["MYSQL_PORT"]),
    username: envs["MYSQL_USER"],
    password: envs["MYSQL_PASSWORD"], // uncomment this line for <8
    db: envs["MYSQL_DATABASE"],
});

/** This is the final config object */
const config: NessieConfig = {
    client,
    migrationFolders: ["./db/migrations"],
    seedFolders: ["./db/seeds"],
};

export default config;
