import { client } from "../db/index.ts";

interface User {
  id: number;
  name: string;
  created_at: string;
}

// TODO: 普通のフレームワークの感覚だとcontextを引数に取りそうだけど・・・
export const indexUser = async () => {
  const users = await client.query<User[]>(`select * from users`);
  return users;
};

export const createUser = async (name: string) => {
  return await client.execute(`INSERT INTO users(name) values(?)`, [name]);
};
