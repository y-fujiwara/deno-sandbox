import { Client } from "https://deno.land/x/mysql/mod.ts";
import * as dotEnv from "https://deno.land/x/dotenv/mod.ts";

const envs = dotEnv.config();

export class WrapperClient extends Client {
  async query<T = any>(sql: string, params?: any[]): Promise<T> {
    const ret = await super.query(sql, params);
    return ret as T;
  }
}

const client: WrapperClient = await new WrapperClient().connect({
  hostname: envs["MYSQL_HOST"],
  username: envs["MYSQL_USER"],
  db: envs["MYSQL_DATABASE"],
  poolSize: 3, // connection limit
  password: envs["MYSQL_PASSWORD"], // uncomment this line for <8
});
export { client };
