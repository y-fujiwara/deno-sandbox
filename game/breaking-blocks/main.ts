import { cookie } from "./deps.ts";
const server = Deno.listen({ port: 8080 });
const sockets: { [uid: string]: WebSocket } = {};

function dateFormat(): string {
  const date = new Date();
  return (
    date.getFullYear() +
    "/" +
    ("0" + (date.getMonth() + 1)).slice(-2) +
    "/" +
    ("0" + date.getDate()).slice(-2) +
    " " +
    ("0" + date.getHours()).slice(-2) +
    ":" +
    ("0" + date.getMinutes()).slice(-2) +
    ":" +
    ("0" + date.getSeconds()).slice(-2) +
    "(JST)"
  );
}

// Note: メモ,サンプル
// https://github.com/socketio/socket.io-adapter/blob/master/lib/index.ts#L231
function broadcast(senderUid: string, payload: string) {
  const broadcastSockets = Object.assign({}, sockets);
  delete broadcastSockets[senderUid];
  for (const socket of Object.values(broadcastSockets)) {
    socket.send(payload);
  }
}

/**
 * front sample:
 * webSocket = new WebSocket("ws://localhost:8080")
 * webSocket.onmessage = (e) => { console.log(e.data); }
 * webSocket.send("test")
 * @param req
 * @returns
 */
function handleSocketAccept(req: Request): Response {
  if (req.headers.get("upgrade") != "websocket") {
    return new Response("request isn't trying to upgrade to websocket.");
  }

  const { socket, response } = Deno.upgradeWebSocket(req);

  socket.onopen = () => {
    const uid = cookie.getCookies(req.headers)["uid"];
    sockets[uid] = socket;
    console.log("socket opened");
  };

  socket.onmessage = (e: MessageEvent<string>) => {
    const uid = cookie.getCookies(req.headers)["uid"];
    broadcast(uid, e.data);
  };

  socket.onerror = (e) => {
    const uid = cookie.getCookies(req.headers)["uid"];
    if (uid) {
      delete sockets[uid];
    }

    console.error("socket errored:", (e as ErrorEvent).message);
  };

  socket.onclose = () => {
    const uid = cookie.getCookies(req.headers)["uid"];
    if (uid) {
      delete sockets[uid];
    }

    console.log("socket closed");
  };

  return response;
}

function handleLogin(req: Request): Response {
  const res = new Response("login");

  const stillUid = cookie.getCookies(req.headers)["uid"];
  if (stillUid) {
    return res;
  }

  const uid = crypto.randomUUID();
  cookie.setCookie(res.headers, { name: "uid", value: uid });
  return res;
}

async function handle(conn: Deno.Conn) {
  const httpConn = Deno.serveHttp(conn);
  for await (const requestEvent of httpConn) {
    console.log(
      `[${dateFormat()}]method=${requestEvent.request.method}, url=${
        requestEvent.request.url
      }`
    );
    const url = new URL(requestEvent.request.url);

    if (url.pathname === "/login") {
      await requestEvent.respondWith(handleLogin(requestEvent.request));
      return;
    }

    if (url.pathname === "/accept") {
      await requestEvent.respondWith(handleSocketAccept(requestEvent.request));
      return;
    }

    await requestEvent.respondWith(
      new Response("Not Found", {
        status: 404,
      })
    );
  }
}

for await (const conn of server) {
  handle(conn);
}
