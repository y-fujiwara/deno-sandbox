/** @jsx h */
import { ComponentChildren, h, Head } from "../deps.ts";
export function Layout({ children }: { children: ComponentChildren }) {
  // TODO: 本来はこのコンポーネントなしで_app.tsxだけにしたいがwrapperのdivが消されたりするのでワークアラウンドで利用
  return (
    <div class="container">
      <div class="mt-5">
        {children}
      </div>
    </div>
  );
}
